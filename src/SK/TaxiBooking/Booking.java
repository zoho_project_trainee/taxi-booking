package SK.TaxiBooking;

import java.util.*;
public class Booking
{
	
	//Book Taxi 
	
	static int distance = 0;
	
    public static void bookTaxi(int customerID,char pickupPoint,char dropPoint,int pickupTime,List<Taxi> freeTaxis)
    {
        int min = 999;
        
        int earning = 0;
        int nextfreeTime = 0;
        char nextSpot = 'Z';
        Taxi bookedTaxi = null;
        String tripDetail = "";
        for(Taxi t : freeTaxis)
        {
            int distance1 = Math.abs(t.currentSpot -pickupPoint ) * 15;
            if(distance1 < min)
            {
            	//System.out.println("Inside Loop");
                bookedTaxi = t;
                distance = Math.abs(dropPoint - pickupPoint) * 15;
   
                earning = (distance-5) * 10 + 100;
             
                int dropTime  = pickupTime + distance/15;

                nextfreeTime = dropTime;

                nextSpot = dropPoint;
                
                tripDetail = customerID + "               " + customerID + "          " + pickupPoint +  "      " + dropPoint + "       " + pickupTime + "          " +dropTime + "           " + earning;
                min = distance1;
            }
            
        }

        
        bookedTaxi.setDetails(true,nextSpot,nextfreeTime,bookedTaxi.totalEarnings + earning,tripDetail);
        System.out.println("Customer ID : "+customerID);
        System.out.println("Alloted Taxi : Taxi - " + bookedTaxi.id );

    }

    //create Taxi
    
    public static List<Taxi> createTaxis(int n)
    {
        List<Taxi> taxis = new ArrayList<Taxi>();
        
        for(int i=1 ;i <=n;i++)
        {
            Taxi t = new Taxi();
            taxis.add(t);
        }
        return taxis;
    }

    // to find the free taxis
    
    public static List<Taxi> getFreeTaxis(List<Taxi> taxis,int pickupTime,char pickupPoint)
    {
        List<Taxi> freeTaxis = new ArrayList<Taxi>();
        for(Taxi t : taxis)
        {   
            if(t.freeTime <= pickupTime && (Math.abs(t.currentSpot - pickupPoint ) <= pickupTime - t.freeTime))//To check whether it will reach in given time
            freeTaxis.add(t);
        }
        return freeTaxis;
    }


    public static void main(String[] args)
    {

        List<Taxi> taxis = createTaxis(4);

        Scanner s = new Scanner(System.in);
        int id = 1;

        while(true)
        {
        System.out.println("_________________________________Menu___________________________________");
        System.out.println("1 - > Taxi Creation");
        System.out.println("2 - > Book Taxi");
        System.out.println("3- > Print Taxi details");
        int choice = s.nextInt();
        switch(choice)
        {
        case 2:
        {
        
        int customerID = id;
        System.out.println("Pickup Point : ");
        char pickupPoint = s.next().charAt(0);
        System.out.println("Drop Point : ");
        char dropPoint = s.next().charAt(0);
        System.out.println("Pickup time : ");
        int pickupTime = s.nextInt();

        if(pickupPoint < 'A' || dropPoint > 'F' || pickupPoint > 'F' || dropPoint < 'A')
        {
            System.out.println("Valid pickup and drop are A, B, C, D, E, F. Exitting");
            return;
        }
        List<Taxi> freeTaxis = getFreeTaxis(taxis,pickupTime,pickupPoint);

        if(freeTaxis.size() == 0)
        {
            System.out.println("No Taxi can be alloted. Exitting");
            return;
        }    

        // SORTING BASED ON EARNINGS
        
		/*
		 * for(int i = 0;i<freeTaxis.size();i++) { for(int j=0; j<freeTaxis.size();j++)
		 * {
		 * 
		 * } }
		 */
        
        bookTaxi(id,pickupPoint,dropPoint,pickupTime,freeTaxis);
        id++;
        break;
        }
        case 3:
        {
             for(Taxi t : taxis)
                t.printDetails();
            break;
        }
        default:
            return;
        }
      
        }
       
    }
}